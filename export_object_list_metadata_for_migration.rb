require_relative 'lib/migration_metadata_table'

object_pid_list = ARGV[0]
output_file_path = ARGV[1]

puts "Writing migration metadata for objects in #{object_pid_list} to '#{output_file_path}'"

pids = File.readlines(object_pid_list).map(&:chomp)
migration_metadata_table = MigrationMetadataTable.new(pids)
migration_metadata_table.write_to_file(output_file_path)
