class MigrationMetadata

  attr_reader :object

  EXTERNAL_FILE_DATASTREAMS = [ Ddr::Datastreams::CAPTION, Ddr::Datastreams::CONTENT,
                                Ddr::Datastreams::INTERMEDIATE_FILE, Ddr::Datastreams::MULTIRES_IMAGE,
                                Ddr::Datastreams::STREAMABLE_MEDIA ].freeze

  MANAGED_FILE_DATASTREAMS = [ Ddr::Datastreams::EXTRACTED_TEXT, Ddr::Datastreams::FITS,
                               Ddr::Datastreams::STRUCT_METADATA, Ddr::Datastreams::THUMBNAIL ].freeze

  def initialize(object)
    @object = object
  end

  def migration_metadata
    admin_metadata.merge(desc_metadata).merge(relationships).merge(system_data).merge(external_files)
  end

  def admin_metadata
    Ddr::Datastreams::AdministrativeMetadataDatastream.term_names.inject({}) do |memo, term|
      memo[term] = term_values(Ddr::Datastreams::ADMIN_METADATA, term)
      memo
    end.reject { |k,v| v.empty? }
  end

  def desc_metadata
    Ddr::Datastreams::DescriptiveMetadataDatastream.term_names.inject({}) do |memo, term|
      memo[term] = term_values(Ddr::Datastreams::DESC_METADATA, term)
      memo
    end.reject { |k,v| v.empty? }
  end

  def external_files
    files = {}
    EXTERNAL_FILE_DATASTREAMS.each do |dsid|
      if object.datastreams[dsid].present?
        ds = object.datastreams[dsid]
        munged_dsid = underscore_dsid(dsid)
        files["#{munged_dsid}_file_path"] = ds.file_path
        files["#{munged_dsid}_checksum"] = external_file_checksum(object.pid, dsid) # from FileDigest
        files["#{munged_dsid}_checksum_type"] = Ddr::Datastreams::CHECKSUM_TYPE_SHA1 # FileDigest stores SHA-1 checksum
        files["#{munged_dsid}_mime_type"] = ds.mimeType
      end
    end
    files
  end

  def relationships
    rels = {}
    rels[:admin_policy] = object.admin_policy.present? ? object.admin_policy.pid : nil
    rels[:attached_to] = object.respond_to?(:attached_to) && object.attached_to.present? ?
                             object.attached_to.pid : nil
    rels[:external_target_for] = object.is_a?(Target) && object.collection.present? ? object.collection.pid : nil
    rels[:parent] = object.parent.present? ? object.parent.pid : nil
    rels.reject { |k,v| v.nil? }
  end

  def system_data
    sd = {}
    sd[:create_date] = object.create_date
    sd[:model] = object.class.name
    sd[:modified_date] = object.modified_date
    sd[:pid] = object.pid
    sd
  end

  private

  def external_file_checksum(pid, dsid)
    FileDigest.where(repo_id: pid, file_id: dsid).first&.sha1
  end

  def underscore_dsid(dsid)
    dsid.underscore
  end

  def term_values(dsid, term)
    if term == :format
      object.datastreams[dsid].format
    else
      object.datastreams[dsid].send(term)
    end
  end

end
