require_relative 'lib/collection_migration'

collection_pid_list = ARGV[0]
output_file_dir = ARGV[1]

puts "Writing migration metadata for collections in #{collection_pid_list} to '#{output_file_dir}'"

pids = File.readlines(collection_pid_list).map(&:chomp)

pids.each do |pid|
  filename = "#{pid.gsub(':', '_')}.txt"
  output_file = File.join(output_file_dir, filename)
  puts "Writing migration metadata for collection #{pid} to '#{output_file}'"
  CollectionMigration.call(pid, output_file)
end
