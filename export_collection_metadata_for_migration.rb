require_relative 'lib/collection_migration'

collection_pid = ARGV[0]
output_file = ARGV[1]

puts "Writing migration metadata for collection #{collection_pid} to '#{output_file}'"

CollectionMigration.call(collection_pid, output_file)
